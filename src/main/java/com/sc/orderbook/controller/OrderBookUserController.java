package com.sc.orderbook.controller;

import com.sc.orderbook.data.UserClosedOrderBook;
import com.sc.orderbook.data.UserOpenedOrderBook;
import com.sc.orderbook.data.OrdersIdentifier;
import com.sc.orderbook.service.UserOrderBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("user")
public class OrderBookUserController {

    @Autowired
    private UserOrderBookService<UserOpenedOrderBook, OrdersIdentifier, Optional<UserClosedOrderBook>> orderBookService;

    @PostMapping (value="orderbook/orders" , produces= { "application/avro+json" }, consumes= { "application/avro+json" })
    public OrdersIdentifier addOrderBook(@RequestBody UserOpenedOrderBook orderBook) {
        return orderBookService.addOrderToBook(orderBook);
    }

    @GetMapping(value="orderbook/query/{id}" , produces= { "application/avro+json" })
    public UserClosedOrderBook queryClosedOrderBook(@PathVariable("id") long id) {
        Optional<UserClosedOrderBook> userClosedOrderBook = orderBookService.queryOrderBookFromInstrumentId(id);
        if (userClosedOrderBook.isPresent())
            return userClosedOrderBook.get();
        else
            return UserClosedOrderBook.newBuilder().setId(0).build();
    }

}
