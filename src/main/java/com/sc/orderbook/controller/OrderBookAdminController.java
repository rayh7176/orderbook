package com.sc.orderbook.controller;

import com.sc.orderbook.data.AdminCloseOrderBook;
import com.sc.orderbook.data.OrderBookIdentifier;
import com.sc.orderbook.service.AdminOrderBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("admin")
public class OrderBookAdminController {

    @Autowired
    private AdminOrderBookService<OrderBookIdentifier, AdminCloseOrderBook> adminOrderBookService;

    @GetMapping(value="orderbook/{inst}" , produces= { "application/avro+json" })
    public OrderBookIdentifier openOrderBook(@PathVariable("inst") String instrument) {
        return adminOrderBookService.createOrderBookFromInstrumentId(instrument);
    }

    @PostMapping (value="orderbook/close" , produces= { "application/avro+json" }, consumes= { "application/avro+json" })
    public ResponseEntity<Void> closeOrderBook(@RequestBody AdminCloseOrderBook adminOrderBook) {
        adminOrderBookService.closeOrderBook(adminOrderBook);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }
}
