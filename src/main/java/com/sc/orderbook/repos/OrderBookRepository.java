package com.sc.orderbook.repos;

import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class OrderBookRepository<T> {
    Map<Long, T>  store;

    public OrderBookRepository(){
        store = new ConcurrentHashMap<>();
    }

    public boolean addOrderBookToStore(Long key, T value) {
        return (store.putIfAbsent(key, value) == null);
    }

    public void replaceOrderBookInStore(Long key, T value) {
        store.replace(key, value);
    }

    public Optional<T> removeOrderBookToStore(long key) {
        return Optional.ofNullable(store.remove(key));
    }

    public Optional<T> queryOrderBookToStore(long key) {
        return Optional.ofNullable(store.get(key));
    }
}
