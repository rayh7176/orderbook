package com.sc.orderbook.schema;


import org.apache.commons.lang3.SerializationException;

public interface Serializer<T> {
    /**
     * Serialize object as byte array.
     * @param data the object to serialize
     * @return byte[]
     */
    byte[] serialize(T data) throws SerializationException;
}
