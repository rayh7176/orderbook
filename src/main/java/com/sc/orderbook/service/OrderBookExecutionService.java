package com.sc.orderbook.service;

import com.sc.orderbook.data.AdminCloseOrderBook;
import com.sc.orderbook.data.UserClosedOrderBook;
import com.sc.orderbook.data.UserOpenedOrderBook;

public interface OrderBookExecutionService {
    UserClosedOrderBook executionOrder(UserOpenedOrderBook userOrderBook, AdminCloseOrderBook adminOrderBook);
}
