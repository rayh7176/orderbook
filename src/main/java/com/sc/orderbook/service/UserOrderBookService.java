package com.sc.orderbook.service;

public interface UserOrderBookService<B, D, Q> {

    D addOrderToBook(B orderBook);

    Q queryOrderBookFromInstrumentId(long instrument);
}
