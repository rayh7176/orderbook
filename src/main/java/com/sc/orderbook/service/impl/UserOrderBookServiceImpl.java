package com.sc.orderbook.service.impl;

import com.sc.orderbook.data.*;
import com.sc.orderbook.repos.OrderBookRepository;
import com.sc.orderbook.service.UserOrderBookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class UserOrderBookServiceImpl  implements UserOrderBookService<UserOpenedOrderBook, OrdersIdentifier, Optional<UserClosedOrderBook>> {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    OrderBookRepository<UserOpenedOrderBook> openedOrderBookRepository;

    @Autowired
    OrderBookRepository<UserClosedOrderBook> closedOrderBookRepository;


    @Override
    public OrdersIdentifier addOrderToBook(UserOpenedOrderBook orderBook) {
        List<OrderId> orderIds = new ArrayList<>();
        OrdersIdentifier.Builder builder = OrdersIdentifier.newBuilder();
        builder.setBookId(orderBook.getId());
        OrderId.Builder orderIdBuilder = OrderId.newBuilder();
        List<Order> orders = orderBook.getOrders();
        orders.stream().forEach((order) -> {
            orderIds.add(orderIdBuilder.setOrderId(order.getId()).build());
        });
        builder.setOrderIds(orderIds);
        openedOrderBookRepository.replaceOrderBookInStore(orderBook.getId(), orderBook);
        OrdersIdentifier identifier = builder.build();
        LOGGER.info("Generated from order book the OrderIds=" + identifier);
        return identifier;
    }

    @Override
    public Optional<UserClosedOrderBook> queryOrderBookFromInstrumentId(long id) {
        return closedOrderBookRepository.queryOrderBookToStore(id);
    }
}
