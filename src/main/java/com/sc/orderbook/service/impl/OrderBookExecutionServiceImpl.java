package com.sc.orderbook.service.impl;

import com.sc.orderbook.data.*;
import com.sc.orderbook.service.OrderBookExecutionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.sc.orderbook.data.UserClosedOrderBook.newBuilder;

@Service
public class OrderBookExecutionServiceImpl implements OrderBookExecutionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderBookExecutionServiceImpl.class);

    public UserClosedOrderBook executionOrder(UserOpenedOrderBook userOrderBook, AdminCloseOrderBook adminOrderBook){
        double closePrice = adminOrderBook.getPrice();
        double closeQuantity = adminOrderBook.getQuantity();
        UserClosedOrderBook.Builder builder = newBuilder();

        double sumMarketOrderQuantity = userOrderBook.getOrders().stream().filter((order) -> order.getType().compareTo(OrderType.MARKET) == 0 && order.getQuantity() > 0).collect(Collectors.summingInt((o) -> o.getQuantity()));
        double sumLimitOrderQuantity = userOrderBook.getOrders().stream().filter((order) -> (order.getType().compareTo(OrderType.LIMIT) == 0 && order.getPrice() >= closePrice && order.getQuantity() > 0)).collect(Collectors.summingInt((o) -> o.getQuantity()));

        if (sumMarketOrderQuantity==0 && sumLimitOrderQuantity==0) {
            LOGGER.warn("EMPTY ORDERS PLACED");
            // return an empty order
            return new UserClosedOrderBook();
        }

        double percentageMarketOrders =   sumMarketOrderQuantity / (sumLimitOrderQuantity + sumMarketOrderQuantity);
        double percentageLimitOrders = sumLimitOrderQuantity / (sumLimitOrderQuantity + sumMarketOrderQuantity);

        double roundOffMarketOrders = (double) Math.round(percentageMarketOrders * 100) / 100;
        double roundOffLimitOrders = (double) Math.round(percentageLimitOrders * 100) / 100;

        Map<OrderType, List<Order>> orders = userOrderBook.getOrders().stream().
                                collect(Collectors.groupingBy(Order::getType, Collectors.toList()));

        List<cOrder> cOrders = new ArrayList<>();
        for(Map.Entry<OrderType, List<Order>> order : orders.entrySet()) {
            switch (order.getKey()) {
                case MARKET: {
                    cOrders.addAll(populateExecutedOrder(order.getValue(), closeQuantity, roundOffMarketOrders, closePrice, false));
                    break;
                }
                case LIMIT: {
                    cOrders.addAll(populateExecutedOrder(order.getValue(), closeQuantity, roundOffLimitOrders, closePrice, true));
                    break;
                }
            }
        }
        builder.setId(userOrderBook.getId());
        builder.setInstrument(userOrderBook.getInstrument());
        builder.setOrders(cOrders);
        return builder.build();
    }

    private List<cOrder> populateExecutedOrder(List<Order> order, double closeQuantity, double percentage, double closePrice, boolean isLimitOrder){
        return order.stream().map( (o) -> {
                    cOrder.Builder orderBuilder = cOrder.newBuilder();
                    orderBuilder.setId(o.getId());
                    orderBuilder.setSide(o.getSide());
                    if (isLimitOrder && (o.getPrice() <= closePrice || o.getQuantity() == 0 )) {
                        orderBuilder.setQuantity(0);
                        orderBuilder.setStatus(StatusType.INVALID);
                    } else {
                        Double dQuantity = closeQuantity * percentage;
                        orderBuilder.setQuantity(dQuantity.intValue());
                        orderBuilder.setStatus(StatusType.EXECUTED);
                    }
                    orderBuilder.setPrice(closePrice);
                    return orderBuilder.build();
                }
        ).collect(Collectors.toList());

    }
}
