package com.sc.orderbook.service.impl;

import com.sc.orderbook.data.*;
import com.sc.orderbook.repos.OrderBookRepository;
import com.sc.orderbook.service.AdminOrderBookService;
import com.sc.orderbook.service.OrderBookExecutionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.sc.orderbook.data.OrderBookIdentifier.newBuilder;


@Service
public class AdminOrderBookServiceImpl implements AdminOrderBookService<OrderBookIdentifier, AdminCloseOrderBook> {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    OrderBookExecutionService orderBookExecution;

    @Autowired
    OrderBookRepository<UserOpenedOrderBook> openedOrderBookRepository;

    @Autowired
    OrderBookRepository<UserClosedOrderBook> closedOrderBookRepository;

    @Override
    public OrderBookIdentifier createOrderBookFromInstrumentId(String instrument) {
        OrderBookIdentifier.Builder builder = newBuilder();
        long identity = System.nanoTime();

        //Place an empty book in storage
        UserOpenedOrderBook.Builder bookbuilder = UserOpenedOrderBook.newBuilder();
        bookbuilder.setInstrument(instrument);
        bookbuilder.setId(identity);
        if(openedOrderBookRepository.addOrderBookToStore(identity, bookbuilder.build())) {
            builder.setId(identity);
        } else
            builder.setId(identity);
        return builder.build();
    }

    @Override
    public void closeOrderBook(AdminCloseOrderBook adminOrderBook) {
        // Get Data from repository
        Optional<UserOpenedOrderBook> userOpenedOrderBook = openedOrderBookRepository.removeOrderBookToStore(adminOrderBook.getBookId());
        if (userOpenedOrderBook.isPresent()) {
            UserClosedOrderBook userClosedOrderBook = orderBookExecution.executionOrder(userOpenedOrderBook.get(), adminOrderBook);
            closedOrderBookRepository.addOrderBookToStore(adminOrderBook.getBookId(), userClosedOrderBook);
        }
        else
            LOGGER.warn("NO ORDERs available for ORDERBOOK id="+adminOrderBook.getBookId());
    }

}
