package com.sc.orderbook.service;

public interface AdminOrderBookService<T, D> {

    T createOrderBookFromInstrumentId(String instrument);

    void closeOrderBook(D d);
}
