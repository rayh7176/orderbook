Feature: Odrder Book

 @bookingServer
 Background: set the number of messages to send 1
 Given the test is to place

 Scenario: create an order book for an instrument
    Given Server Up and running
    When I have an instrument for the input
    Then return a unique identifier

 Scenario: users add buy orders to the selected open order books
    Given Server Up and running
    When an order has quantity, type and price set
    Then users get a unique order identifier

 Scenario: Admin user closes an order book
    Given Server Up and running
    When price and total quantity set
    Then execute limit orders and market orders using an algorithm

 Scenario: user check status of it's orders
    Given Server Up and running
    When user make the request with order book id
    Then user receive list of it's orders