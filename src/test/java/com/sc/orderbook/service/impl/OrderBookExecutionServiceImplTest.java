package com.sc.orderbook.service.impl;

import com.sc.orderbook.data.*;
import com.sc.orderbook.service.OrderBookExecutionService;
import com.sc.orderbook.service.impl.OrderBookExecutionServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.sc.orderbook.data.AdminCloseOrderBook.newBuilder;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
public class OrderBookExecutionServiceImplTest {

    private OrderBookExecutionService orderBookExecutionService;
    UserOpenedOrderBook userOrderBook;
    AdminCloseOrderBook adminOrderBook;

    /**
     Example (for a single order book)
     Orders:
     id: 1, quantity: 50, type: limit, price: 17.0
     id: 2, quantity: 130, type: limit, price: 14.0
     id: 3, quantity: 150, type: market

     Execution (order book closure):
     quantity: 80, price: 15.0
     */
    @Test
    public void testExecutionOrder_with_two_valid_orders(){
        //GIVEN
        long expectedBookId = 123;
        String expectedInstrument = "eur_usd";

        orderBookExecutionService = new OrderBookExecutionServiceImpl();
        AdminCloseOrderBook.Builder builder = newBuilder();
        builder.setQuantity(80);
        builder.setBookId(expectedBookId);
        builder.setPrice(15.00);
        adminOrderBook = builder.build();
        List<Order> orders = new ArrayList<>();

        UserOpenedOrderBook.Builder builder1 = UserOpenedOrderBook.newBuilder();
        builder1.setId(123);
        builder1.setInstrument(expectedInstrument);

        Order.Builder order1 = Order.newBuilder();
        order1.setQuantity(50);
        order1.setType(OrderType.LIMIT);
        order1.setSide(OrderSide.BUY);
        order1.setId(1);
        order1.setPrice(17.0);
        orders.add(order1.build());

        Order.Builder order2 = Order.newBuilder();
        order2.setQuantity(130);
        order2.setType(OrderType.LIMIT);
        order2.setSide(OrderSide.BUY);
        order2.setId(2);
        order2.setPrice(14.0);
        orders.add(order2.build());

        Order.Builder order3 = Order.newBuilder();
        order3.setQuantity(150);
        order3.setType(OrderType.MARKET);
        order3.setSide(OrderSide.BUY);
        order3.setId(3);
        orders.add(order3.build());

        builder1.setOrders(orders);

        //When
        userOrderBook = builder1.build();
        UserClosedOrderBook actualUserClosedOrderBook = orderBookExecutionService.executionOrder(userOrderBook, adminOrderBook);
        System.out.println(actualUserClosedOrderBook.toString());

        //Then
        List<cOrder> actualOrders = actualUserClosedOrderBook.getOrders();
        StatusType expectedTypeExecuted = StatusType.EXECUTED;
        StatusType actualTypeInvalid = StatusType.INVALID;

        StatusType actualType1 = actualOrders.get(0).getStatus();
        StatusType actualType2 = actualOrders.get(1).getStatus();
        StatusType actualType3 = actualOrders.get(2).getStatus();

        int expectedQuantity1 = 60;
        int expectedQuantity2 = 20;
        int expectedQuantity3 = 0;

        int actualQuantity1 = actualOrders.get(0).getQuantity();
        int actualQuantity2 = actualOrders.get(1).getQuantity();
        int actualQuantity3 = actualOrders.get(2).getQuantity();

        assertThat(actualUserClosedOrderBook.getId(), is(expectedBookId));
        assertThat(actualUserClosedOrderBook.getInstrument(), is(expectedInstrument));
        assertThat(actualOrders.size(), is(3));

        assertThat(actualType1, is(expectedTypeExecuted));
        assertThat(actualType2, is(expectedTypeExecuted));
        assertThat(actualType3, is(actualTypeInvalid));

        assertThat(actualQuantity1, is(expectedQuantity1));
        assertThat(actualQuantity2, is(expectedQuantity2));
        assertThat(actualQuantity3, is(expectedQuantity3));
    }

    @Test
    public void testExecutionOrder_with_only_valid_market_orders(){
        //GIVEN
        long expectedBookId = 123;
        String expectedInstrument = "eur_usd";

        orderBookExecutionService = new OrderBookExecutionServiceImpl();
        AdminCloseOrderBook.Builder builder = newBuilder();
        builder.setQuantity(80);
        builder.setBookId(expectedBookId);
        builder.setPrice(16.00);
        adminOrderBook = builder.build();
        List<Order> orders = new ArrayList<>();

        UserOpenedOrderBook.Builder builder1 = UserOpenedOrderBook.newBuilder();
        builder1.setId(123);
        builder1.setInstrument(expectedInstrument);

        Order.Builder order1 = Order.newBuilder();
        order1.setQuantity(50);
        order1.setType(OrderType.LIMIT);
        order1.setSide(OrderSide.BUY);
        order1.setId(1);
        order1.setPrice(15.0);
        orders.add(order1.build());

        Order.Builder order2 = Order.newBuilder();
        order2.setQuantity(130);
        order2.setType(OrderType.LIMIT);
        order2.setSide(OrderSide.BUY);
        order2.setId(2);
        order2.setPrice(14.0);
        orders.add(order2.build());

        Order.Builder order3 = Order.newBuilder();
        order3.setQuantity(150);
        order3.setType(OrderType.MARKET);
        order3.setSide(OrderSide.BUY);
        order3.setId(3);
        orders.add(order3.build());

        builder1.setOrders(orders);

        //When
        userOrderBook = builder1.build();
        UserClosedOrderBook actualUserClosedOrderBook = orderBookExecutionService.executionOrder(userOrderBook, adminOrderBook);
        System.out.println(actualUserClosedOrderBook.toString());

        //Then
        List<cOrder> actualOrders = actualUserClosedOrderBook.getOrders();
        StatusType expectedTypeExecuted = StatusType.EXECUTED;
        StatusType expectedTypeInvalid = StatusType.INVALID;

        StatusType actualType1 = actualOrders.get(0).getStatus();
        StatusType actualType2 = actualOrders.get(1).getStatus();
        StatusType actualType3 = actualOrders.get(2).getStatus();

        int expectedQuantity1 = 80;
        int expectedQuantity2 = 0;
        int expectedQuantity3 = 0;

        int actualQuantity1 = actualOrders.get(0).getQuantity();
        int actualQuantity2 = actualOrders.get(1).getQuantity();
        int actualQuantity3 = actualOrders.get(2).getQuantity();

        assertThat(actualUserClosedOrderBook.getId(), is(expectedBookId));
        assertThat(actualUserClosedOrderBook.getInstrument(), is(expectedInstrument));
        assertThat(actualOrders.size(), is(3));

        assertThat(actualType1, is(expectedTypeExecuted));
        assertThat(actualType2, is(expectedTypeInvalid));
        assertThat(actualType3, is(expectedTypeInvalid));

        assertThat(actualQuantity1, is(expectedQuantity1));
        assertThat(actualQuantity2, is(expectedQuantity2));
        assertThat(actualQuantity3, is(expectedQuantity3));
    }

    @Test
    public void testExecutionOrder_with_two_valid_limit_orders(){
        //GIVEN
        long expectedBookId = 123;
        String expectedInstrument = "eur_usd";

        orderBookExecutionService = new OrderBookExecutionServiceImpl();
        AdminCloseOrderBook.Builder builder = newBuilder();
        builder.setQuantity(80);
        builder.setBookId(expectedBookId);
        builder.setPrice(14.00);
        adminOrderBook = builder.build();
        List<Order> orders = new ArrayList<>();

        UserOpenedOrderBook.Builder builder1 = UserOpenedOrderBook.newBuilder();
        builder1.setId(123);
        builder1.setInstrument(expectedInstrument);

        Order.Builder order1 = Order.newBuilder();
        order1.setQuantity(50);
        order1.setType(OrderType.LIMIT);
        order1.setSide(OrderSide.BUY);
        order1.setId(1);
        order1.setPrice(15.0);
        orders.add(order1.build());

        Order.Builder order2 = Order.newBuilder();
        order2.setQuantity(130);
        order2.setType(OrderType.LIMIT);
        order2.setSide(OrderSide.BUY);
        order2.setId(2);
        order2.setPrice(16.0);
        orders.add(order2.build());

        Order.Builder order3 = Order.newBuilder();
        order3.setQuantity(150);
        order3.setType(OrderType.MARKET);
        order3.setSide(OrderSide.BUY);
        order3.setId(3);
        orders.add(order3.build());

        builder1.setOrders(orders);

        //When
        userOrderBook = builder1.build();
        UserClosedOrderBook actualUserClosedOrderBook = orderBookExecutionService.executionOrder(userOrderBook, adminOrderBook);
        System.out.println(actualUserClosedOrderBook.toString());

        //Then
        List<cOrder> actualOrders = actualUserClosedOrderBook.getOrders();
        StatusType expectedTypeExecuted = StatusType.EXECUTED;
        StatusType expectedTypeInvalid = StatusType.INVALID;

        StatusType actualType1 = actualOrders.get(0).getStatus();
        StatusType actualType2 = actualOrders.get(1).getStatus();
        StatusType actualType3 = actualOrders.get(2).getStatus();

        int expectedQuantity1 = 36;
        int expectedQuantity2 = 44;
        int expectedQuantity3 = 44;

        int actualQuantity1 = actualOrders.get(0).getQuantity();
        int actualQuantity2 = actualOrders.get(1).getQuantity();
        int actualQuantity3 = actualOrders.get(2).getQuantity();

        assertThat(actualUserClosedOrderBook.getId(), is(expectedBookId));
        assertThat(actualUserClosedOrderBook.getInstrument(), is(expectedInstrument));
        assertThat(actualOrders.size(), is(3));

        assertThat(actualType1, is(expectedTypeExecuted));
        assertThat(actualType2, is(expectedTypeExecuted));
        assertThat(actualType3, is(expectedTypeExecuted));

        assertThat(actualQuantity1, is(expectedQuantity1));
        assertThat(actualQuantity2, is(expectedQuantity2));
        assertThat(actualQuantity3, is(expectedQuantity3));
    }

    @Test
    public void testExecutionOrder_with_two_valid_empty_orders(){
        //GIVEN
        long expectedBookId = 123;
        String expectedInstrument = "eur_usd";

        orderBookExecutionService = new OrderBookExecutionServiceImpl();
        AdminCloseOrderBook.Builder builder = newBuilder();
        builder.setQuantity(80);
        builder.setBookId(expectedBookId);
        builder.setPrice(14.00);
        adminOrderBook = builder.build();
        List<Order> orders = new ArrayList<>();

        UserOpenedOrderBook.Builder builder1 = UserOpenedOrderBook.newBuilder();
        builder1.setId(123);
        builder1.setInstrument(expectedInstrument);

        Order.Builder order1 = Order.newBuilder();
        order1.setQuantity(0);
        order1.setType(OrderType.LIMIT);
        order1.setSide(OrderSide.BUY);
        order1.setId(1);
        order1.setPrice(15.0);
        orders.add(order1.build());

        Order.Builder order2 = Order.newBuilder();
        order2.setQuantity(0);
        order2.setType(OrderType.LIMIT);
        order2.setSide(OrderSide.BUY);
        order2.setId(2);
        order2.setPrice(16.0);
        orders.add(order2.build());

        Order.Builder order3 = Order.newBuilder();
        order3.setQuantity(150);
        order3.setType(OrderType.MARKET);
        order3.setSide(OrderSide.BUY);
        order3.setId(3);
        orders.add(order3.build());

        builder1.setOrders(orders);

        //When
        userOrderBook = builder1.build();
        UserClosedOrderBook actualUserClosedOrderBook = orderBookExecutionService.executionOrder(userOrderBook, adminOrderBook);
        System.out.println(actualUserClosedOrderBook.toString());

        //Then
        List<cOrder> actualOrders = actualUserClosedOrderBook.getOrders();
        StatusType expectedTypeExecuted = StatusType.EXECUTED;
        StatusType expectedTypeInvalid = StatusType.INVALID;

        StatusType actualType1 = actualOrders.get(0).getStatus();
        StatusType actualType2 = actualOrders.get(1).getStatus();
        StatusType actualType3 = actualOrders.get(2).getStatus();

        int expectedQuantity1 = 80;
        int expectedQuantity2 = 0;
        int expectedQuantity3 = 0;

        int actualQuantity1 = actualOrders.get(0).getQuantity();
        int actualQuantity2 = actualOrders.get(1).getQuantity();
        int actualQuantity3 = actualOrders.get(2).getQuantity();

        assertThat(actualUserClosedOrderBook.getId(), is(expectedBookId));
        assertThat(actualUserClosedOrderBook.getInstrument(), is(expectedInstrument));
        assertThat(actualOrders.size(), is(3));

        assertThat(actualType1, is(expectedTypeExecuted));
        assertThat(actualType2, is(expectedTypeInvalid));
        assertThat(actualType3, is(expectedTypeInvalid));

        assertThat(actualQuantity1, is(expectedQuantity1));
        assertThat(actualQuantity2, is(expectedQuantity2));
        assertThat(actualQuantity3, is(expectedQuantity3));
    }
}
