package com.sc.orderbook.cucumber.steps;

import com.sc.orderbook.OrderBookApplication;
import com.sc.orderbook.cucumber.run.OrderBookDefs;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.springframework.boot.SpringApplication;
import org.springframework.test.context.ContextConfiguration;

import java.net.HttpURLConnection;

@ContextConfiguration(classes=OrderBookDefs.class)
public class OrderBookSteps {

    private HttpURLConnection httpURLConnection;

    @Given("^Server Up and running$")
    public void the_test_is_connected_to_the_Server() throws Throwable {
    }

    @Given ("^ I have an instrument  \"([^\"]*)\" for the input$")
    public void send_request_with_instrument(String instrument) throws Throwable {
    }

    @Given("^a collection of calc is loaded from the \"([^\"]*)\" sheet$")
    public void a_collection_of_calc_is_loaded_from_the_sheet(String fileName) throws Throwable {
        //csvTofileJob
    }

    @Before("~@bookingServer")
    public void bookingServerSetUp (){
        SpringApplication.run(OrderBookApplication.class, new String[]{});
    }

    @Then("return a unique identifier")
    public void return_booking_number (){
    }
}
