package com.sc.orderbook.cucumber.run;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan(basePackages = {"com.cucumber.jms.*", "com.cucumber.client.*"})
@PropertySource("classpath:orderbook-test.properties")
public class OrderBookDefs {

    @Value("$jms.context.factory")
    private String jmsContextFactory;

    @Bean
    static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }


}
