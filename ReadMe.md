Please don't use the product in a commercial env without my consent.
For more details please contact me on rayh176@gmail.com

This example is used to provide the following
1. Admin Create an open book
2. Client place orders
3. Admin close the order book
4. Client query the order book


SWAGGER -ACCESS
http://localhost:8090/swagger-ui.html#

I have added an instrument and BUY or SELL side to an order book in order to provide a better view of our book.
From step one, please copy the id generated and use it for step 2,3,4 as a unique identifier to our order.
The unique id is generated using nanotime instead of UUID.

the below steps should be used in the right order to actually open & close an order book

1. Admin user opens an order book for a given currency
>>>> Via postman
+ Header Content-Type: application/avro+json
+ localhost:8090/admin/orderbook/inst=gbp_dev


2. User buy orders to the selected open book.
>>>> Via postman

+ localhost:8090/user/orderbook/orders

+ Header Content-Type: application/avro+json

+ Body
{
    "instrument": "gbp_usd",
    "id": 2641038099162,
    "Orders": [
        {
            "id": 1,
            "quantity": 50,
            "price": 17.0,
            "type": "LIMIT",
            "side": "BUY"
        },
        {
            "id": 2,
            "quantity": 130,
            "price": 14.0,
            "type": "LIMIT",
            "side": "BUY"
        },
        {
            "id": 3,
            "quantity": 150,
            "price": 0.0,
            "type": "MARKET",
            "side": "BUY"
        }
    ]
}

+ curl --location --request POST 'http://localhost:8090/content-types/' \
--header 'Content-Type: application/avro+json' \
--data-raw '{
	"name":"Post",
	{
        "instrument": "gbp_usd",
        "id": 2641038099162,
        "Orders": [
            {
                "id": 1,
                "quantity": 50,
                "price": 17.0,
                "type": "LIMIT",
                "side": "BUY"
            },
            {
                "id": 2,
                "quantity": 130,
                "price": 14.0,
                "type": "LIMIT",
                "side": "BUY"
            },
            {
                "id": 3,
                "quantity": 150,
                "price": 0.0,
                "type": "MARKET",
                "side": "BUY"
            }
        ]
    }
}'

3- Execution of order book closure:
   >>>>>> Via Postman
   localhost:8090/admin/orderbook/close
   Headers Content-Type: application/avro+json
   Body
   {
       "bookId": 27265311533174,
       "quantity": 80,
       "price": 15.0
   }


4- Query Closed book with the Id
   id=27265311533174 is a unique number generated, please replace the Id
   >>>>>> Via Postman
   localhost:8090/user/orderbook/query/27265311533174

Note:
+ Cucumber project not completed
+ Reason having 5 avro files as I wasn't able to stop null value displaying in the returned JSON object
  require to add value 'setDeSerializationInclusion(JsonInclude.Include.NON_NULL)' to stop Json displaying NULL values